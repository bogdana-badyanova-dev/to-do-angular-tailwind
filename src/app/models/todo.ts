import { Guid } from 'guid-typescript';

export interface Todo {
    id: Guid;
    title: string;
    description: string;
    deadline: string | null | undefined;
    isCompleted: boolean;
    created_at: Date;
    todos: Todo[];
}

export interface TodoCreateDto {
    title: string;
    description: string;
    deadline: string | null | undefined;
}
