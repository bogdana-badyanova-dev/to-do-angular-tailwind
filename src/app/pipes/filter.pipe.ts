import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(items: any[], field: string, value: boolean): number {
        return items.filter(
            it => it[field] === value
        ).length;
    }

}
