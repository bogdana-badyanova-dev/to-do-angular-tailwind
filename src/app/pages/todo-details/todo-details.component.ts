import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { Todo } from 'src/app/models/todo';
import { ToDoService } from 'src/app/services/to-do/to-do.service';

@Component({
    selector: 'app-todo-details',
    templateUrl: './todo-details.component.html',
    styleUrls: ['./todo-details.component.scss']
})
export class TodoDetailsComponent implements OnInit {

    todoId!: Guid;
    todo!: Todo;
    todos: Todo[] = [];

    constructor(private route: ActivatedRoute, private todoService: ToDoService, private router: Router) { }

    private reload(path: string, id: Guid = Guid.create()) {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate(["/" + path, id]));
    }

    public ngOnInit() {
        this.todoId = this.route.snapshot.params['id'];
        this.todo = this.todoService.getById(this.todoId);
    }

    public delete(id: Guid) {
        this.todoService.delete(id);
        this.reload("home");
    }

    public deleteNested(id: Guid, parentId: Guid) {
        this.todoService.deleteNested(id, parentId);
        this.reload("todo", parentId);
    }

    public complete(id: Guid) {
        this.todoService.complete(id);
        this.reload("todo", id);
    }

    public completeNested(id: Guid, parentId: Guid) {
        this.todoService.completeNested(id, parentId);
        this.reload("todo", parentId);
    }

}
