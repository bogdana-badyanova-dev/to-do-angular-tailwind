import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Todo, TodoCreateDto } from 'src/app/models/todo';
import { ToDoService } from 'src/app/services/to-do/to-do.service';
import { Guid } from 'guid-typescript';
import { Router } from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public todos: Todo[] = [];

    todoForm = new FormGroup({
        title: new FormControl('', [
            Validators.required,
            Validators.minLength(2)
        ]),
        description: new FormControl(''),
        deadline: new FormControl('')
    });

    constructor(private todoService: ToDoService, private router: Router) { }

    public ngOnInit() {
        this.todos = this.todoService.getAll();
    }

    private reload(path: string, id: Guid = Guid.create()) {
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate(["/" + path, id]));
    }

    public countCompleted(item: Todo) {
        return item.isCompleted;
    }

    public create() {
        if (this.todoForm.valid) {
            let todo: TodoCreateDto = {
                title: this.todoForm.value.title + "",
                description: this.todoForm.value.description + "",
                deadline: this.todoForm.value.deadline
            }

            this.todoService.create(todo);
            this.todoForm.reset();
        } else this.todoForm.controls.title.markAsTouched();
    }

    public createNested(id: Guid) {
        if (this.todoForm.valid) {
            let todo: TodoCreateDto = {
                title: this.todoForm.value.title + "",
                description: this.todoForm.value.description + "",
                deadline: this.todoForm.value.deadline
            }
            this.todoService.createNested(id, todo);
            this.todoForm.reset();
        } else this.todoForm.controls.title.markAsTouched();
    }

    public complete(id: Guid) {
        this.todoService.complete(id);
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate(["/home"]));
    }

    public delete(id: Guid) {
        this.todoService.delete(id);
        this.reload("home");
    }

    public deleteCompleted() {
        this.todoService.deteleCompleted();
        this.reload("home");
    }

}
