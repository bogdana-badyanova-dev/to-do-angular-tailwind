import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo, TodoCreateDto } from 'src/app/models/todo';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { Guid } from 'guid-typescript';

@Injectable({
    providedIn: 'root'
})
export class ToDoService {

    private todos: Todo[] = [];

    constructor(private localStorage: LocalStorageService, private http: HttpClient) {
        this.todos = this.getAll();
    }

    public getAll(): Todo[] {
        if (this.localStorage.getData('todos') !== "") {
            this.todos = JSON.parse(this.localStorage.getData('todos'));
        }
        return this.todos;
    }

    public getById(id: Guid): Todo {
        var todoArray = JSON.parse(this.localStorage.getData('todos'));
        return todoArray.filter((todo: Todo) => todo.id === id).pop();
    }

    public complete(id: Guid) {
        let index = this.todos.findIndex(item => item.id === id);
        if (index == -1) {
            console.log("Такой элемент не найден");
        } else {
            this.todos[index].isCompleted = !this.todos[index].isCompleted;
            let thisIsCompleted = this.todos[index].isCompleted;

            // меняем статус всех вложенных на родительский
            if (this.todos[index].todos.length > 0) {
                let nested = this.todos[index].todos;
                nested.forEach(todo => {
                    todo.isCompleted = thisIsCompleted;
                });
            }

            this.localStorage.saveData('todos', JSON.stringify(this.todos))
        }
    }

    private checkCompleted(parentIndex: number) {
        let nested = this.todos[parentIndex].todos;
        if (nested.length == 0) return;

        let isAllNestedSame = true;
        let firstNestedIdCompleted = nested[0].isCompleted;

        // проверяем все ли вложенные стали одинаковые
        nested.forEach(todo => {
            if (todo.isCompleted != firstNestedIdCompleted) {
                isAllNestedSame = false;
                return;
            }
        });

        // меняем статус родительского элемента
        if (isAllNestedSame) {
            this.todos[parentIndex].isCompleted = firstNestedIdCompleted;
        } else {
            this.todos[parentIndex].isCompleted = false;
        }
    }

    public completeNested(id: Guid, parentId: Guid) {
        let parentIndex = this.todos.findIndex(item => item.id === parentId);
        if (parentIndex == -1) {
            console.log("Такой элемент не найден");
        } else {
            let index = this.todos[parentIndex].todos.findIndex(item => item.id === id);

            if (index != -1) {
                this.todos[parentIndex].todos[index].isCompleted = !this.todos[parentIndex].todos[index].isCompleted;

                this.checkCompleted(parentIndex);

                this.localStorage.saveData('todos', JSON.stringify(this.todos))
            } else
                console.log("Такой дочерний элемент не найден");
        }
    }

    public create(todo: TodoCreateDto): void {
        let newTodo: Todo = {
            id: (Guid.create()).toJSON().value,
            title: todo.title,
            description: todo.description,
            deadline: todo.deadline,
            isCompleted: false,
            created_at: new Date(),
            todos: []
        }
        this.todos.push(newTodo);
        this.localStorage.saveData('todos', JSON.stringify(this.todos))
    }

    public createNested(parentId: Guid, todo: TodoCreateDto): void {
        let newTodo: Todo = {
            id: (Guid.create()).toJSON().value,
            title: todo.title,
            description: todo.description,
            deadline: todo.deadline,
            isCompleted: false,
            created_at: new Date(),
            todos: []
        }
        let parentIndex = this.todos.findIndex(item => item.id === parentId);
        this.todos[parentIndex].todos.push(newTodo);
        this.localStorage.saveData('todos', JSON.stringify(this.todos))
    }

    public delete(id: Guid): void {
        var todoArray = JSON.parse(this.localStorage.getData('todos'));
        for (var index in todoArray) {
            if (todoArray[index].id === id) {
                todoArray.splice(index, 1);
                this.localStorage.saveData('todos', JSON.stringify(todoArray));
            }
        }
    }

    public deleteNested(id: Guid, parentId: Guid): void {
        let parentIndex = this.todos.findIndex(item => item.id == parentId);
        let nestedIndex = this.todos[parentIndex].todos.findIndex(item => item.id == id);
        this.todos[parentIndex].todos.splice(nestedIndex, 1);
        this.checkCompleted(parentIndex);
        this.localStorage.saveData('todos', JSON.stringify(this.todos));
    }

    public deteleCompleted() {
        this.todos = this.todos.filter((todo) => !todo.isCompleted);
        this.localStorage.saveData('todos', JSON.stringify(this.todos));
    }
}
