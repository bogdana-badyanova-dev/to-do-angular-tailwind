/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{html,ts}",
    ],
    theme: {
        screens: {
            sm: '420px',
            md: '600px',
            lg: '900px',
            xl: '1440px',
        },
        colors: {
            'dark': '#282C33',
            'pink': '#C778DD',
            'white': '#ffffff',
            'grey': '#ABB2BF',
            'error': '#CF6679'
        }
    },
    plugins: [],
}
